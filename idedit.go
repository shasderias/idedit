package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

const (
	delim = "---\n"
)

type Config struct {
	Target      string `json:"target"`
	Marker      string `json:"marker"`
	StartMarker []byte
	EndMarker   []byte
	Lines       []byte
}

func printUsage() {
	fmt.Fprintf(os.Stderr, "idedit [filename]\n")
}

func main() {
	args := os.Args
	if len(args) != 2 {
		printUsage()
		os.Exit(1)
	}

	filename := args[1]

	conf, err := parseIdedit(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error loading idedit file: %v\n", err)
		os.Exit(1)
	}

	if err := conf.Apply(); err != nil {
		fmt.Fprintf(os.Stderr, "error applying changes: %v\n", err)
		os.Exit(1)
	}
}

func parseIdedit(filename string) (*Config, error) {
	f, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("error reading %s: %v", filename, err)
	}

	delimIdx := bytes.Index(f, []byte(delim))

	var conf Config

	if err := json.Unmarshal(f[:delimIdx], &conf); err != nil {
		return nil, fmt.Errorf("error parsing config %s: %v", filename, err)
	}

	conf.Lines = bytes.TrimSpace(f[delimIdx+len(delim):])
	conf.StartMarker = bytes.Replace([]byte(conf.Marker), []byte("{MARK}"), []byte("START"), -1)
	conf.EndMarker = bytes.Replace([]byte(conf.Marker), []byte("{MARK}"), []byte("END"), -1)

	return &conf, nil
}

func (c *Config) Apply() error {
	if _, err := os.Stat(c.Target); os.IsNotExist(err) {
		buf := make([]byte, 0)

		buf = append(buf, c.managedBlock()...)
		buf = tidy(buf)

		return ioutil.WriteFile(c.Target, buf, 0644)
	}

	f, err := ioutil.ReadFile(c.Target)
	if err != nil {
		return err
	}

	sIdx := bytes.Index(f, c.StartMarker)
	eIdx := bytes.Index(f, c.EndMarker)

	switch {
	case sIdx == -1 && eIdx >= 0:
		fallthrough
	case sIdx >= 0 && eIdx == -1:
		return errors.New("start or end marker present without the other")
	case sIdx > eIdx:
		return errors.New("start marker after end marker")
	case sIdx == -1 && eIdx == -1:
		// append
		nf, err := backupAndCreate(c.Target)
		if err != nil {
			return err
		}
		defer nf.Close()

		buf := make([]byte, 0)

		buf = append(buf, f...)
		buf = append(buf, c.managedBlock()...)
		buf = tidy(buf)

		if _, err := nf.Write(buf); err != nil {
			return err
		}
	default:
		nf, err := backupAndCreate(c.Target)
		if err != nil {
			return err
		}
		defer nf.Close()

		buf := make([]byte, 0)

		buf = append(buf, f[:sIdx]...)
		buf = append(buf, c.managedBlock()...)
		buf = append(buf, f[eIdx+len(c.EndMarker):]...)
		buf = tidy(buf)

		if _, err := nf.Write(buf); err != nil {
			return err
		}
	}

	return nil
}

func (c *Config) Remove(filename string) error {
	// TODO
	return nil
}

func (c *Config) managedBlock() []byte {
	buf := bytes.Buffer{}
	buf.Grow(2 + len(c.StartMarker) + 1 + len(c.Lines) + 1 + len(c.EndMarker) + 2)

	buf.WriteRune('\n')
	buf.WriteRune('\n')
	buf.Write(c.StartMarker)
	buf.WriteRune('\n')
	buf.Write(c.Lines)
	buf.WriteRune('\n')
	buf.Write(c.EndMarker)
	buf.WriteRune('\n')
	buf.WriteRune('\n')

	return buf.Bytes()
}

func backupAndCreate(filename string) (*os.File, error) {
	stat, err := os.Stat(filename)
	if err != nil {
		return nil, err
	}

	bakFilename := filename + ".bak"

	_ = os.Remove(bakFilename)

	if err := os.Rename(filename, filename+".bak"); err != nil {
		return nil, err
	}

	return os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_EXCL, stat.Mode())
}

func tidy(b []byte) []byte {
	l := len(b)

	for {
		b = bytes.ReplaceAll(b, []byte("\n\n\n"), []byte("\n\n"))
		if len(b) == l {
			break
		}
		l = len(b)
	}

	b = bytes.TrimSpace(b)

	if len(b) != 0 {
		b = append(b, '\n')
	}

	return b
}
