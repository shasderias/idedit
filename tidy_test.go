package main

import (
	"bytes"
	"testing"
)

func TestTidy(t *testing.T) {
	testCases := []struct {
		Name     string
		Input    string
		Expected string
	}{
		{
			Name:     "simple",
			Input:    "\n\n\n\n",
			Expected: "",
		},
		{
			Name:     "content before",
			Input:    "abc\n\n\ndef\n",
			Expected: "abc\n\ndef\n",
		},
		{
			Name:     "leading whitespace",
			Input:    "  \n\tabc\n",
			Expected: "abc\n",
		},
		{
			Name:     "trailing whitespace",
			Input:    "abc\t\n  ",
			Expected: "abc\n",
		},
		{
			Name:     "end of file newline",
			Input:    "abc",
			Expected: "abc\n",
		},
		{
			Name:     "end of file newline 2",
			Input:    "abc\n\n\n",
			Expected: "abc\n",
		},
		{
			Name:     "empty file",
			Input:    "",
			Expected: "",
		},
		{
			Name:     "single character",
			Input:    "a",
			Expected: "a\n",
		},
		{
			Name:     "all together now",
			Input:    "   abc\n\n\ndef\n\n\n\n\nghi \t\n",
			Expected: "abc\n\ndef\n\nghi\n",
		},
	}

	for _, tc := range testCases {
		ti := tidy([]byte(tc.Input))
		ex := []byte(tc.Expected)
		if !bytes.Equal(ti, ex) {
			t.Logf("%s: expected %q got %q instead", tc.Name, tc.Expected, ti)
			t.Fail()
		}
	}
}
